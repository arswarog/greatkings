<?php
/* @var $this UnitconveerController */
/* @var $model Unitconveer */

$this->breadcrumbs=array(
	'Unitconveers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Unitconveer', 'url'=>array('index')),
	array('label'=>'Manage Unitconveer', 'url'=>array('admin')),
);
?>

<h1>Create Unitconveer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>