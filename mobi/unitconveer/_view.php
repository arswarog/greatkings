<?php
/* @var $this UnitconveerController */
/* @var $data Unitconveer */
?>

<div class="pall">
	<a href="<?php echo url('/unit/view',array('id'=>$data->unit)) ?>">
		<span class="el70 fll"><img width="64" height="64" class="fll" src="/img/units/<?php echo $data->unit ?>.jpg" alt=""></span>
		<span class="bl" style="font-weight: bold">
			<?php echo $data->unitinfo->title ?>			
			<span class="col1"><?php echo $data->count; ?> ед.</span>
		</span>
		<span class="bl">Осталось <span class="col1"><?php echo timeto($data->endtime) ?></span></span>
	</a>
	<span class="bl">
		<?php if ($showCity) { ?>
		Город <?php echo CHtml::link($data->city->title, array('//unitconveer', 'city'=>$data->cityid), array('class'=>'col1')); ?>
		<?php } ?>
	</span>
	<span class="bl"><?php echo CHtml::link('Закончить обучение - '.$data->donate().'<img src="/img/silver.gif"/>', array('//unitconveer/donate', 'id'=>$data->id), array('class'=>'col1')); ?></span>
	<div class="flc"></div>
</div>


<div class="sep-light"></div>

<div class="sep-dark"></div>
