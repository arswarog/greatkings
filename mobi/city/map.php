<?php
/* @var $this CityController */
/* @var $city City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	$city->title,
);

$this->pagetitle='Карта';

yii::app()->widgets->portlet('main', 'mainmenu', 'Меню', 'zii.widgets.CMenu', array('items'=>$this->menu));

//$city->res=Res::model()->findByPk($city->resources);
//var_dump($city->res);
echo $map->city->title;
?>

<div>
	<a class="loc bl" href="<?php echo url('/city/view', array('id'=>$map->city->id)) ?>">
		<div id="blik" class="blik" style="display: inline-block; width: 0px; left: 10px; background-position: 100% 50%;"></div>
		<span class="hlpt">Карта</span>
		<span class="hlpd col1"></span>
	</a>
	<div class="flc"></div>
</div>

<div class="displ-t"><div class="displ-l"><div class="displ-r"><div class="displ-b"><div class="displ-tl"><div class="displ-tr"><div class="displ-bl"><div class="displ-br"><div class="displ"><?php 
$this->widget('MapMobile',array(
	'pos'=>$map->pos,
	'link'=>'/city/place',
	'up'=>2,
	'down'=>2,
	'left'=>2,
	'right'=>2	
)); //*/
?></div></div></div></div></div></div></div></div></div>

<div class="title">Армии на территории</div>
<?php 
$dataProvider=new CArrayDataProvider($map->armies);
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'/army/_view',
	'template'=>'{items}{pager}',
	'emptyText'=>'На этой территории армий нет'
));
?>

<div class="title">Отправить армию</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'map-view-form',
	'enableAjaxValidation'=>false,
	'method'=>'get',
	'action'=>array('//army/go')
)); ?>

	<div class="row"><?php echo CHtml::dropDownList('id',null,Army::armiesToRun($pos)).CHtml::hiddenField('pos',$pos); ?>
	<?php echo CHtml::submitButton('Отправить'); ?>
	</div>

<?php $this->endWidget(); ?>

