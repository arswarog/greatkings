<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Города'=>array('index'),
	'Основание',
);

$this->menu=array(
	array('label'=>'List City', 'url'=>array('index')),
	array('label'=>'Manage City', 'url'=>array('admin')),
);
?>

<h1>Основание города</h1>


<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'city-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php 
	if ($msg) echo $model->addError('a', $msg);
	echo $form->errorSummary($model);?>

	<div class="row">
		Название города: <?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<?php /*<div class="row">
		Поселенцев: <?php echo $form->textField($model,'poselencev',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'poselencev'); ?>
	</div> */ ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Основать'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->