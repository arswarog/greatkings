<?php
/* @var $this HeroController */
/* @var $model Hero */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uid'); ?>
		<?php echo $form->textField($model,'uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'army'); ?>
		<?php echo $form->textField($model,'army'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hp'); ?>
		<?php echo $form->textField($model,'hp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hpmax'); ?>
		<?php echo $form->textField($model,'hpmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mp'); ?>
		<?php echo $form->textField($model,'mp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mpmax'); ?>
		<?php echo $form->textField($model,'mpmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'level'); ?>
		<?php echo $form->textField($model,'level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'exp'); ?>
		<?php echo $form->textField($model,'exp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expmax'); ?>
		<?php echo $form->textField($model,'expmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parameters'); ?>
		<?php echo $form->textArea($model,'parameters',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->