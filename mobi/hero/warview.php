<?php
/* @var $this HeroController */
/* @var $model Hero */

$this->breadcrumbs=array(
	'Heroes'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Hero', 'url'=>array('index')),
	array('label'=>'Create Hero', 'url'=>array('create')),
	array('label'=>'Update Hero', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Hero', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hero', 'url'=>array('admin')),
);
?>

<div class="pall">
	<a href="<?php echo url('/hero/view',array('id'=>$model->id)) ?>">
		<span class="el70 fll"><img width="64" height="64" class="fll" src="/img/hero/<?php echo $model->herobase ?>.jpg" alt=""></span>
		<span class="bl">
			<?php echo $model->title ?> 
			[<?php echo CHtml::encode($model->level); ?>]
			<span class="col1"><?php echo Hero::$typelist[$model->type] ?>
		</span></span>
		<div class="bl178"><?php echo Html::hpbar($model) ?></div>
	</a>
	<span class="bl"><?php echo $model->more ?></span>
	<div class="flc"></div>
</div>

<div class="title">Умения</div>

<?php
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($model->skills),
	'itemView'=>'//skill/_warview',
	'template'=>'{items}{pager}',
	'viewData'=>array('warid'=>$model->id),
)); ?>