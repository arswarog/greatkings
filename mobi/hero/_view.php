<div class="pall">
	<a href="<?php echo url('/hero/view',array('id'=>$data->id)) ?>">
		<span class="el70 fll"><img width="64" height="64" class="fll" src="/img/hero/<?php echo $data->herobase ?>.jpg" alt=""></span>
		<span class="bl">
			<?php echo Hero::$typelist[$data->type].' <span class="col1">'.$data->title.'</span>';
            echo ' ['.CHtml::encode($data->level).'] '; ; ?></a>
			<?php if (!$hidearmy) { echo '<div class="col1">'.CHtml::link($data->army->title, array('/army/view', 'id'=>$data->armyid),array('class'=>'col1')) ?></div><?php } ?>
		</span>
		<div class="bl70"><?php echo Html::hpbar($data) ?></div>
		<div class="flc"></div>

	<span class="bl"><?php echo $data->more ?></span>
	<div class="flc"></div>
</div>

<div class="sep-light"></div>

<div class="sep-dark"></div>
