<?php
/* @var $this HeroController */
/* @var $model Hero */

$this->breadcrumbs=array(
	'Heroes'=>array('index'),
	$model->title,
);

$this->pagetitle=$model->title;
?>
<div class="title"><?php echo $model->title; ?> [<?php echo CHtml::encode($model->level); ?>]</div>

<div class="pall">
	<a href="<?php echo url('/hero/view',array('id'=>$model->id)) ?>">
		<span class="el70 fll"><img width="64" height="64" class="fll" src="/img/hero/<?php echo $model->herobase ?>.jpg" alt=""></span>
		<span class="bl"></a>
			<div class="col1"><?php echo Hero::$typelist[$model->type].' армии '.CHtml::link($model->army->title, array('/army/view', 'id'=>$model->armyid),array('class'=>'col1')) ?></div>
		</span>
		<span class="bl"><img src="<?php echo $model->hpUrl() ?>" /></span>
		<div class="flc"></div>

	<span class="bl"><?php echo $model->more ?></span>
	<div class="flc"></div>
</div>
<table width="100%">
<tr>
	<td><div class="title">Возможности</div></td>
	<td><div class="title">Бонус</div><td>
</tr>
<tr style="vertical-align: top;"><td>
	<table width="100%">
	<?php foreach ($model->paramlist as $n=>$v)
		echo '<tr><td>'.$v.'</td><td class="col1 r">'.$model->params[$n].'</td></tr>'; ?>
	</table>
</td><td>
	<table width="100%">
	<tr class="col1"><td style="text-align: right">Урон</td><td></td><td>Защита</td></tr>
	<?php $list=array_merge($model->damage, $model->protect);
	reset($list);
	for (;(list($n,$v)=each($list));)
	{
		echo '<tr>';
		$v=0;
		if (isset($model->damage[$n])) $v=1;
		if (isset($model->protect[$n])) $v=1;
		if ($v)
			echo '<td style="text-align: right">'.$model->damage[$n].'</td>'
				.'<td style="text-align: center"><img src="/img/'.$n.'.png"/></td>'
				.'<td>'.$model->protect[$n].'</td>';
		else
			echo '<td colspan=3></td>';

		echo '</tr>';
	}
	?></table>
	<?php //foreach ($model->cost as $n=>$v) echo '<img src="/img/res/'.$n.'.png" />'.$v.' '; ?>
	<span class="bl"><?php echo $model->more ?></span>
	<div class="flc"></div>
</td></tr>
</table>


<div class="sep-light"></div><div class="sep-dark"></div>
Свободного опыта: <?php echo $model->freeexp ?><?php echo CHtml::beginForm(); ?>
<?php echo CHtml::dropdownlist('Hero[param]',null,$model->paramlist); ?>
<?php echo CHtml::textField('Hero[count]', $model->freeexp); ?>
<?php echo CHtml::submitButton('Ok'); ?>
<?php echo CHtml::endForm(); ?>
</form>



<div class="title">Умения</div>
<div><?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>new CArrayDataProvider($model->skills),
	'itemView'=>'//skill/_view',
	'template'=>'{items}{pager}',
)); ?></div>

