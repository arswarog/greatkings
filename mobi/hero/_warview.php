<?php
/* @var $this HeroController */
/* @var $data Hero */
?>

<div class="pall">
	<a href="<?php echo url('/hero/warview',array('id'=>$data->id)) ?>">
		<span class="el70 fll"><img width="64" height="64" class="fll" src="/img/hero/<?php echo $data->herobase ?>.jpg" alt=""></span>
		<span class="bl">
			<?php echo $data->title ?> 
			[<?php echo CHtml::encode($data->level); ?>]
			<span class="col1"><?php echo Hero::$typelist[$data->type] ?>
		</span></span>
		<div class="bl70"><?php echo Html::hpbar($data) ?></div>
	</a>
	<span class="bl"><?php echo $data->more ?></span>
	<div class="flc"></div>
</div>

<div class="sep-light"></div>

<div class="sep-dark"></div>
