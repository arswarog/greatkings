<?php
/* @var $this HeroController */
/* @var $data Hero */
?>

<div class="pall">
	<a href="<?php echo url('/hero/herobase',array('id'=>$data->id)) ?>">
		<span class="el70 fll"><img width="64" height="64" class="fll" src="{{domain}}/img/hero/<?php echo $data->id ?>.jpg" alt=""></span>
		<span class="bl">
			<?php echo $data->title ?> 
			<span class="col1"><?php echo Hero::$typelist[$data->type] ?>
		</span></span>
		<span class="bl">HP:<?php echo $data->hpmax.' MP:'.$data->mpmax ?></span>
	</a>
	<span class="bl"><?php echo $data->more ?></span>
	<div class="bl title"><a href="<?php echo url('/hero/buyhero', array('id'=>$data->id)) ?>">Нанять (<?php 
	if ($data->cost) echo "<img src=\"/img/res/gold.gif\"/>".$data->cost.' ';
	if ($data->costgold) echo "<img src=\"/img/silver.gif\"/>".$data->costgold.' ';
	?>)</a></div>
	<div class="flc"></div>
</div>

<div class="sep-light"></div>

<div class="sep-dark"></div>
