<?php
/* @var $this HeroController */
/* @var $model Hero */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'hero-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'uid'); ?>
		<?php echo $form->textField($model,'uid'); ?>
		<?php echo $form->error($model,'uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'army'); ?>
		<?php echo $form->textField($model,'army'); ?>
		<?php echo $form->error($model,'army'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hp'); ?>
		<?php echo $form->textField($model,'hp'); ?>
		<?php echo $form->error($model,'hp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hpmax'); ?>
		<?php echo $form->textField($model,'hpmax'); ?>
		<?php echo $form->error($model,'hpmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mp'); ?>
		<?php echo $form->textField($model,'mp'); ?>
		<?php echo $form->error($model,'mp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mpmax'); ?>
		<?php echo $form->textField($model,'mpmax'); ?>
		<?php echo $form->error($model,'mpmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'level'); ?>
		<?php echo $form->textField($model,'level'); ?>
		<?php echo $form->error($model,'level'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exp'); ?>
		<?php echo $form->textField($model,'exp'); ?>
		<?php echo $form->error($model,'exp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parameters'); ?>
		<?php echo $form->textArea($model,'parameters',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'parameters'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->