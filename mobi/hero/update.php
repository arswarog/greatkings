<?php
/* @var $this HeroController */
/* @var $model Hero */

$this->breadcrumbs=array(
	'Heroes'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hero', 'url'=>array('index')),
	array('label'=>'Create Hero', 'url'=>array('create')),
	array('label'=>'View Hero', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Hero', 'url'=>array('admin')),
);
?>

<h1>Update Hero <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>