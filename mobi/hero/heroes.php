<?php
/* @var $this HeroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Heroes',
);

$this->pagetitle='Герои';
?>

<div class="title">Нанять героя</div>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$taverna,
	'itemView'=>'_herobase',
	'template'=>'{items}{pager}',
	'emptyText'=>'В таверне пока никого нет'
)); ?>
