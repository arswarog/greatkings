<?php
/* @var $this ChatController */
/* @var $model Chat */
/* @var $form CActiveForm */

if ($model==null)
	$model=Chat::$chat;
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'chat-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textField($model,'text',array('size'=>60,'maxlength'=>1024)); ?>

	<?php echo CHtml::submitButton('Отправить'); ?>

<?php $this->endWidget(); ?>
