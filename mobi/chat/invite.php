<?php
/* @var $this ChatController */
/* @var $model Chat */

$this->breadcrumbs=array(
	'Chats'=>array('index'),
	$model->time,
);

$this->pagetitle='Чат '.$title;
?>

<div>
	<a class="loc bl" href="<?php echo url('/chat/room', array('room'=>Chat::encode($room))) ?>">
		<div id="blik" class="blik" style="display: inline-block; width: 0px; left: 10px; background-position: 100% 50%;">Комната</div>
		<span class="hlpt"><?php echo $title ?></span>
		<span class="hlpd"></span>
	</a>
	<div class="flc"><img src="" /></div>
</div>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$friends,
	'itemView'=>'_invitelist',
	'template'=>'{items}{pager}',
	'emptyText'=>'<div class="c">У Вас нет друзей</div>',
	'viewData'=>array(
		'room'=>Chat::encode($room)
	)
)); 

?>
