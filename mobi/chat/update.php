<?php
/* @var $this ChatController */
/* @var $model Chat */

$this->breadcrumbs=array(
	'Chats'=>array('index'),
	$model->time=>array('view','id'=>$model->time),
	'Update',
);

$this->menu=array(
	array('label'=>'List Chat', 'url'=>array('index')),
	array('label'=>'Create Chat', 'url'=>array('create')),
	array('label'=>'View Chat', 'url'=>array('view', 'id'=>$model->time)),
	array('label'=>'Manage Chat', 'url'=>array('admin')),
);
?>

<h1>Update Chat <?php echo $model->time; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>