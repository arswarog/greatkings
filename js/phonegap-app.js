/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var phoneGapApp = {
  // Application Constructor
  initialize: function () {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function () {
    document.addEventListener('load', this.onLoad, false);
    document.addEventListener('deviceready', this.onDeviceReady, false);
	document.addEventListener("menubutton", this.onMenuButton, false);
	document.addEventListener("online", this.online, false);
	document.addEventListener("offline", this.offline, false);
//    window.addEventListener("orientationchange", orientationChange, true);
	  alert('init');
  },
  // load Event Handler
  //
  onLoad: function () {
    console.log("On load");
	alert('onLoad');
    //FastClick.attach(document.body);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'app.receivedEvent(...);'
  onDeviceReady: function () {
	  alert('ready');
  	  angular.bootstrap(document, ['greatkings']);
	  alert('ready ok');
  },
  onMenuButton: function ()
  {
	  alert('menu');
	/*	function alertDismissed() {
			// do something
		}

		function onPrompt(results) {
			alert("You selected button number " + results.buttonIndex + " and entered " + results.input1);
		}

		navigator.notification.prompt(
			'Please enter your name',  // message
			onPrompt,                  // callback to invoke
			'Registration',            // title
			['Ok','Exit'],             // buttonLabels
			'Jane Doe'                 // defaultText
		);
		alert('end');*/
  },
  online: function (){
	  alert('online');
  },
  offline: function (){
	  alert('offline');
  },
  // Update DOM on a Received Event
  receivedEvent: function (id) {
    alert(id);
  }
};