'use strict';

/* Directives */

angular.module('greatkings.directives', [])
.directive('compile', ['$compile', function ($compile) {
	return function(scope, element, attrs) {
	scope.$watch(
		function(scope) {
			return scope.$eval(attrs.compile);
		},
		function(value) {
			element.html(value);
			$compile(element.contents())(scope);
		}
	)};
}])
.directive('hpbar', function($rootScope) {
	return {
		restrict: 'E',
		templateUrl: 'bars/hp.html',
		replace: true,
		scope: {
			hp: '@',
			hpmax: '@',
			mp: '@',
			mpmax: '@',
			exp: '@',
			expmax: '@',
			click: '@'
		},
	}
})
.directive('mapview', function(GkData, $location, $cookies) {
	return {
		restrict: 'E',
		templateUrl: 'map/map.html',
		replace: true,
		// передача двух атрибутов из attrs в область видимости шаблона
		scope: {
			position: '=',
			place: '=',
			update: '=',
			h: '@',
			w: '@',
			movie: '@',
			link: '@'
		},
		link: function($scope, element, attrs) {
	//			console.log(attrs);
			function refresh(value)
			{
				var w=parseInt($scope.w);
				var h=parseInt($scope.h);
				if (undefined===value)
					value=$cookies.get('lastMap');
				if (undefined===value)
					value='1:1:1';
				var pos=value.split(':');
				pos={
					w: w,
					h: h,
					x: parseInt(pos[0]),
					y: parseInt(pos[1]),
					z: parseInt(pos[2]),
					lp: parseInt(pos[0])-w,
					rp: parseInt(pos[0])+w,
					up: parseInt(pos[1])+h,
					dp: parseInt(pos[1])-h,
				};
				$scope.pos=pos;
				$scope.setPlace(pos.x,pos.y,false);
				$scope.rows=[]; for (var i=pos.up; i>=pos.dp; i--) $scope.rows.push(i);
				$scope.cols=[]; for (var i=pos.lp; i<=pos.rp; i++) $scope.cols.push(i);

				var bg={
					lp: Math.floor((pos.lp-1)/4),
					rp: Math.ceil((pos.rp)/4),
					up: Math.ceil((pos.up)/4),
					dp: Math.floor((pos.dp-5)/4),
					bg: ''
				};
				for (var y=bg.dp; y<=bg.up; y++)
				for (var x=bg.lp; x<=bg.rp; x++)
				{
	//					console.log("bg "+x+'.'+y+':');
					var xp=25+x*200-pos.lp*50;
					var yp=175-y*200+pos.dp*50;

					var xx=x; 
					var yy=y; 
					if (xx<0 || yy<0) {xx=0; yy=0;};

					//bg.list[pos.z+'/'+yy+'/'+xx]=xp+' '+yp;
					bg.bg+=',url('+GkData.domain+'/m/'+pos.z+'/'+yy+'/'+xx+'.jpg) no-repeat '+xp+'px '+yp+'px';
				}
				$scope.background=bg.bg;
				$scope.scene='url(../img/null.gif)';

				var qq={
					lp: Math.floor((pos.lp)/10),
					rp: Math.floor((pos.rp)/10),
					up: Math.floor((pos.up)/10),
					dp: Math.floor((pos.dp)/10)
				};

				if (qq.lp<0) qq.lp=0;
				if (qq.dp<0) qq.dp=0;

				function coord(x, y)
				{
					var xp=25+(x-pos.lp)*50;
					var yp=25+(pos.up-y)*50;
					return xp+"px "+yp+"px";
				}

				function upd()
				{
					$scope.setPlace(pos.x,pos.y,false);
					$scope.cell=[];
					var scene='';
					for (var y=qq.dp; y<=qq.up; y++)
					for (var x=qq.lp; x<=qq.rp; x++)
					{
						GkData.get('map', x+":"+y+":"+pos.z, 'full', function(data){
	/** /
							data.x=data.x*10;
							data.y=data.y*10;
							for (var y=0; y<10; y++)
							for (var x=0; x<10; x++)
								scene+=",url(/img/map/hide"+data.vis.substr(y*10+x, 1)+".png) no-repeat "+
								coord(x+data.x, y+data.y);
	//*/						
							for (var i in data.items)
							{
								scene+=",url(../img/"+data.items[i].type+"/"+data.items[i].view+".png) no-repeat ";
								switch (data.items[i].type)
								{
									case 'city': scene+=coord(data.items[i].x-1, data.items[i].y-1+2); break;
									case 'army': scene+=coord(data.items[i].x, data.items[i].y); break;
									case 'map': scene+=coord(data.items[i].x, data.items[i].y); break;
								}
							}
							$scope.scene+=scene;
						});
					}
				};
				GkData.query('map', qq.lp+','+qq.rp+':'+qq.up+','+qq.dp+':'+pos.z, 'square', [], upd, true);
				upd();
				
			};
			$scope.$watch('position', function(value) { refresh(value); });
			$scope.$watch('update', function(value) { refresh($scope.position); });

			$scope.get = function (x,y) {
				var sec={ x: x%10, y: y%10 };
				//$rootScope
			}
			$scope.setPos=function (x, y) {
				if ('false'!=$scope.movie)
					$scope.position=x+':'+y+':'+$scope.pos.z;
			};
			$scope.setPlace=function (x, y, link) {
				if (undefined==link) link=false;
				$scope.place=x+':'+y+':'+$scope.pos.z;
				var xp=25+(x-$scope.pos.lp)*50;
				var yp=25+($scope.pos.up-y)*50;
				$scope.placebg='url(../img/map/current.png) no-repeat '+xp+"px "+yp+"px,";
				if (link && (typeof $scope.link == 'string'))
				{
					var ind=$scope.link.indexOf(':pos');
					if (~ind)
						$location.path($scope.link.substr(0,ind)+$scope.place+$scope.link.substr(ind+4));
				}	
				//$scope.position=$scope.place;
			}

			/*attrs.$observe('caption', function(value) {
				element.find('figcaption').text(value)
			})

			// атрибуты именуются с применением «верблюжьей» нотации
			attrs.$observe('photoSrc', function(value) {
				element.find('img').attr('src', value)
			})*/
		}
	}
})
.directive('warview', function(GkData) {
	return {
	restrict: 'E',
	templateUrl: 'war/map.php',
	replace: true,
	// передача двух атрибутов из attrs в область видимости шаблона
	scope: {
		war: '=',
		anime: '='
	},
	link: function($scope, element, attrs) {
//			console.log(attrs);
		$scope.cols=[];
		$scope.$watch('war', function(war) {
			if (undefined==war) return;
			var w=5;
			var h=war.map.length;
			var pos={
				w: w,
				h: h,
				lp: 0,
				rp: 4,
				up: h-1,
				dp: 0,
			};
			$scope.pos=pos;
			$scope.setPlace(pos.x,pos.y,false);
			$scope.rows=[]; for (var i=pos.up; i>=pos.dp; i--) $scope.rows.push(i);
			$scope.cols=[]; for (var i=pos.lp; i<=pos.rp; i++) $scope.cols.push(i);

			$scope.scene='url(../img/null.gif),';
			$scope.background='url(../img/war/'+war.warplace+'.jpg) -10px -'+(640-(10-h)*50)+'px';
			
			function coord(x, y)
			{
				var xp=25+(x-pos.lp)*50;
				var yp=25+(pos.up-y)*50;
				return xp+"px "+yp+"px";
			}

			$scope.cell=[];
			var scene='';
		});
		$scope.get = function (x,y) {
			var sec={ x: x%10, y: y%10 };
			//$rootScope
		};
		$scope.setPlace=function (x, y, link) {
			if (undefined===link) link=true;
			$scope.place=x+':'+y+':'+$scope.pos.z;
			var xp=25+(x-$scope.pos.lp)*50;
			var yp=25+($scope.pos.up-y)*50;
			//$scope.placebg='url(/img/map/current.png) no-repeat '+xp+"px "+yp+"px,";
			alert(link);
			if (link && (typeof $scope.link === 'string'))
			{
				var ind=$scope.link.indexOf(':pos');
				alert(ind);
				if (~ind)
					$location.path($scope.link.substr(0,ind)+$scope.place+$scope.link.substr(ind+4));
			}	
			//$scope.position=$scope.place;
		}
		/*attrs.$observe('caption', function(value) {
			element.find('figcaption').text(value)
		})

		// атрибуты именуются с применением «верблюжьей» нотации
		attrs.$observe('photoSrc', function(value) {
			element.find('img').attr('src', value)
		})*/
	}
	}
});
