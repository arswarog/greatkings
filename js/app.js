'use strict';
var app=angular.module('greatkings', 
[
	'mobile-angular-ui',
	'greatkings.services',
	'greatkings.filters',
	'greatkings.directives',
	'greatkings.httpProvider',
	'ngRoute',
	'ngCookies'
]);
