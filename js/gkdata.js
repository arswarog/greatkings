'use strict';

angular.module('greatkings.services', ['ngResource'])
.factory('GkData', function($http, $rootScope, $timeout, $interval, $location) {
	var data={};
	var updatesCallback;
	var timerCallback;
	var cachelist={};

	var domain="https://greatkings.ru";

	/*data.ts=0;
	data.onemsgs={};
	data.onemsgcurrent=0;
	data.cities={};
	data.builds={};
	data.myuser={};
	data.dialogs={};
	data.users={};
	data.chats={};	/// список комнта чата
	data.chatlogs={}; /// логи чата
	data.maps={};     /// картографические данные
	*/
	function get(module, id, version, callback)
	{
		if (undefined==data[module])
			return false;
		if (undefined==data[module][id] || !isDataVersion(module, id, version))
		{
			if (undefined!=data[module][id])
				callback(data[module][id]);
			$rootScope.isLoading=true;
			var url=domain+'/api/'+module+'/'+id;
			if ('full'!=version) url+='/'+version;
			url+='?ts='+data.ts;
			$http.get(url).then(function(r) 
			{
				 apply(r.data);
				 callback(data[module][id]);
				 $rootScope.isLoading=false;
				 //$timeout(poller, 10000);
			});
		}
		else
			callback(data[module][id]);

	}

	function paramsTranslate(q)
	{
		for (var j in q)
			switch (q[j])
			{
				case 'myid': q[j]=data.me.id; break;
				case 'myrace': q[j]=data.me.race; break;
			}
	}

	function privateFind(module, q)
	{
		if (undefined==data[module])
			return undefined;

		paramsTranslate(q);
		
		var tmp = [];
		for(var i in data[module])
		{
			var item = data[module][i];
			var add=true;
			for (var j in q)
				if (item[j]!=q[j])
					add=false;
			if (add)
				tmp.push(item);
		}
		return tmp;
	}

	function find(module, q, callback)
	{
		if (undefined==data[module])
			return undefined;

		paramsTranslate(q);

		callback(privateFind(module, q));

		/*$rootScope.isLoading=true;
		$http.post('/api/'+module, q).then(function(r) 
		{
			 apply(r.data);
			 callback(privateFind(module, q));
			 $rootScope.isLoading=false;
		});
		*/
	}

	function query(module, key, q, params, callback, reload)
	{
		if (undefined==data[module])
		{
			console.log('query: module '+module+' not found. Query canseled');
			return undefined;
		}

		if (undefined==params) params={};
		if (undefined==reload) reload=false;
		if (undefined==q) q='';

		paramsTranslate(q);

		if (undefined==cachelist[module]) cachelist[module]={};
		if (undefined==cachelist[module][key]) cachelist[module][key]={};

//		console.log('query '+module+'/'+q);
//		console.log(cachelist);

		if (reload || undefined==cachelist[module][key][q])
		{
			cachelist[module][key][q]={};
			$rootScope.isLoading=true;
			$http.post(domain+'/api/'+module+'/'+key+'/'+q+'?ts='+data.ts, params).then(function(r) 
			{
				 apply(r.data);
				 callback(r.data);
				 cachelist[module][key][q]=data.ts;
				 $rootScope.isLoading=false;
			});
		}
		else
			callback();
	}

	function directQuery(module, key, q, params, callback)
	{
		if (undefined==data[module])
			return undefined;

		if (undefined==params) params={};
		if (undefined==q) q='';

		paramsTranslate(q);

		//if (undefined==cachelist[module]) cachelist[module]={};
		//if (undefined==cachelist[module][key]) cachelist[module][key]={};

//		console.log('directQuery '+module+'/'+q);
		//console.log(cachelist);

		//if (reload || undefined==cachelist[module][key][q])
		{
			//cachelist[module][key][q]={};
			$rootScope.isLoading=true;
			$http.post(domain+'/api/'+module+'/'+key+'/'+q+'?ts='+data.ts, params).then(function(r) 
			{
				 //apply(r.data);
				 callback(r.data);
				 //cachelist[module][key][q]=data.ts;
				 $rootScope.isLoading=false;
			});
		}
		//else
		//	callback();
	}

	var data = { response: {}, calls: 0 };
	var poller = function()
	{
		//if (data.ts>0)
			$http.get(domain+'/api/?ts='+data.ts).then(function(r) {
				apply(r.data);
		//		$timeout(poller, 10000);
			});
		$timeout(poller, 10000);
	};
	
	function setTimer(callback)
	{
		if (typeof callback == 'function')
		{
			timerCallback=callback;
			timerCallback();
		}
		else
			timerCallback=undefined;
	}
	
	var timerEvent=function()
	{
		if (typeof timerCallback == 'function')
			timerCallback();
		$timeout(timerEvent, 100)
	}
	timerEvent();
	
	function isDataVersion(module, id, version)
	{
		if (false==data[module][id]) return true;
		if (undefined==data[module][id]['version']) return true;
//		console.log('isDataVersion '+module+'.'+id+'['+version+']:');
		var v=data[module][id]['version'];
//		console.log(v);
		if (undefined==v) v=[];
		for(var i=0; i<v.length; i++)
			if (v[i] === version) return true;
		return false;
	}
	
	function setDataVersion(module, operation, id, version)
	{
		if (false==data[module][id]) return true;
//		if (undefined==data[module][id]['version']) return true;
		var v=data[module][id]['version'];
		if (!(v instanceof Array))
			v=[];
		if (operation=='set')
		{
			v=[version];
		}
		else if (operation=='add')
		{
			if (!isDataVersion(module, id, version))
			{
				v.push(version);
			}

		}
//		console.log('setDataVersion '+module+'.'+id+'['+operation+' '+version+']:');
//		console.log(v);
		data[module][id]['version']=v;
	}
	
	/**
	* Функция принятия данных
	* 
	*	{
	*		module: {
	*			add: добавляет все подэлементы юнитов модуля
	*			clear: заменяет существующий модуль на клиенте на присланный
	*			replace: заменяет (добавляет) юниты модуля
	*			delete: удаляет униты модуля
	*		}
	* 
	* Если юнита модуля нет - то возвращаем текстовое 'none'
	* 
	* @param {type} data полученные данные с сервера в формате JSON
	* @param {type} $rootScope
	* @returns 
	*/
	function apply(answer)
	{
		if (answer.ts>0)
			syncTimer(answer.ts);
		//else
			//return;
		
		var canupdate=true;
		for (var module in answer)
		{
			if ('ts'==module) continue;
			if ('me'==module) 
			{
				canupdate=true;
				data.me=answer['me'];
				continue;
			}
			if ('return'==module) continue;
			if (undefined==data[module])
			{
				console.log('unknow module "'+module+'". ignored');
				continue;
			}
			if (undefined!=answer[module]['clear']) data[module]={};
				canupdate=true;
			for (name in answer[module])
			{
				
				switch (name)
				{ 
					case 'add': 
						for (var index in answer[module]['add'])
						{
							if (undefined==data[module][index]) data[module][index]={};
							setDataVersion(module, 'add', index, answer[module]['add'][index]['version']);
							for (var n in answer[module]['add'][index])
							{
								if ('version'==n) continue;
//								console.log(module+'.add.'+index+'.'+n);
								data[module][index][n]=answer[module]['add'][index][n];
							}
//							console.log(data[module][index]);
						}
					break;
					case 'replace':
					case 'clear': 
						for (var index in answer[module][name])
						{
//							console.log(module+'.replace.'+index);
							data[module][index]=answer[module][name][index];
							setDataVersion(module, 'set', index, answer[module][name][index]['version']);
						}
					break;
					case 'delete':
						for (var index in answer[module]['replace'])
						{
//							console.log(module+'.delete.'+index);
							data[module][index]='none';
						}
					break;
				}
				$rootScope.$emit('changed.'+module, name);
			}
		}
		
		noticeUpdate();
	
		//$rootScope.debug=data.city;
		$rootScope.myuser=data.me;
		
		if (canupdate==true)
			if (typeof updatesCallback == 'function')
				updatesCallback();
	}

/*	function updates()
	{
		//$rootScope.isLoading=true;
		$http.get('/api/?ts='+data.ts).then(function(r) 
		{
			apply(r.data);
			//$rootScope.isLoading=false;
			$timeout(updates, 1000);
		});
	}	*/

	function login(login, password)
	{
		$http.post(domain+'/api/login', {login: login, password: password}).then(function(r) {
			apply(r.data, $rootScope);
			$rootScope.debug=r;
			$location.path('/');
		});
	}

	function logout()
	{
		init();
		$http.post('/api/logout', {}).then(function(r) {
			apply(r.data, $rootScope);
			$location.path('/login');
		});
	}

	function resetState()
	{
		data.ts=0;
		data.tsd=0;
		data.me={};
		data.my=[];
		data.city=[];
		data.build=[];
		data.army=[];
		data.war=[];
		data.hero=[];
		data.general=[];
		data.unitconveer=[];
		data.unit=[];
		data.map=[];
		data.chatroom=[];
		data.chat=[];
		data.notice=[];
		data.currentnotice=false;
	}
	
	function init()
	{
		resetState();
		poller();
		$rootScope.domain=domain;
		$interval(onTimer, 100);
	}

	function onTimer()
	{
		var cts=new Date().getTime()/1000;
		var t=cts+data.tsd;
		//console.log(cts);
		$rootScope.time=t;
	}

	function syncTimer(ts)
	{
		// FIXME: синхронизация локального и серверного таймера
		data.ts=ts;
		var cts=new Date().getTime()/1000;
		data.tsd=data.ts-cts;
		$rootScope.tsd=data.tsd;
//		console.log('ts:'+data.ts+', tsd:'+data.tsd);
	}

	function noticeYes()
	{
		if (!data.currentnotice) return;
		noticeUpdate(true);
	}
	
	function noticeNo()
	{
		if (!data.currentnotice) return;
		noticeUpdate(true);
	}

	function noticeUpdate(clear)
	{
		if (true==clear) data.currentnotice=false;
		
		if (false==data.currentnotice)
		{
			var notice=false;
			if (undefined!=data.notice[0] && data.notice[0].id==0) {
				notice=data.notice[0];
				delete data.notice[0];
			} else {
				data.notice.sort(function(a,b){ return a.level-b.level; });
				if (undefined!=data.notice[0])
				{
					notice=data.notice[0];
					delete data.notice[0];
				}
			}
			if (10==notice.from)
				;//notice.from+=data.myuser.race;
			data.currentnotice=notice;
			$rootScope.notice=notice;
		}
	}
	init();

	function onUpdates(callback)
	{
		if (typeof callback == 'function')
		{
			updatesCallback=callback;
			updatesCallback();
		}
		else
			updatesCallback=undefined;
	}

	return {
		myuser: data.myuser,
		find: find,
		get: get,
		query: query,
		directQuery: directQuery,
		init: init,
		login: login,
		logout: logout,
		noticeYes: noticeYes,
		noticeNo: noticeNo,
		update: onUpdates,
		timer: setTimer,
		resetState: resetState,
		domain: domain
	};
});
