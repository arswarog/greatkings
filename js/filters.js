'use strict';

angular.module('greatkings.filters', [])
	/*.filter('chatlist', function (items)
	{
		// function to invoke by Angular each time
		// Angular passes in the `items` which is our Array
		return function (items) {
		// Create a new Array
		var filtered = [];
		// loop through existing Array
		for (var i = 0; i < items.length; i++) {
			var item = items[i];
			filtered.push(item);
		}
		// boom, return the Array after iteration's complete
		return filtered;
	};
	})*/
	.filter('lister',function(){
		return function (items, data) {
			
			//console.log('Джина вызывали?')
			//console.log(data)
			var tmp = [];
			for(var i in items)
			{
				var item = items[i];
				var add=true;
				for (var j in data)
					if (item[j]!=data[j])
						add=false;
				if (add)
					tmp.push(item);

			}
			//tmp.sort(function(a, b){ return (a.time<b.time)?1:-1; });
		    return tmp;
		}
	})
	.filter('sorter',function(){
		return function (items, data) {
			items.sort(function(a, b) {
				var r=0;
//				console.log('a or b?');
//				console.log(a);
//				console.log(b);
				for (var j in data)
				{
//					console.log(' by '+j+':'+data[j]);
					if (a[j]==b[j]) continue;
					if (data[j]=='asc') {
						r=(a[j]>b[j])?1:-1;
					} else if (data[j]=='desc') {
						r=(a[j]<b[j])?1:-1;
					} else 
						r=(a[j]==data[j])?-1:1;
//					console.log('  r='+r);
				}
//				console.log('result '+r);
				return r;
			});
		    return items;
		}
	})
	.filter('chatlist',function(){
		return function (items, roomid, limit) {
			//console.log('Джина вызывали?'+roomid)
			if (undefined==limit) limit=10;
			var tmp = [];
			for(var i in items){
				var item = items[i];
				if((roomid==0) || (item['room']==roomid))
					tmp.push(item);
			}
			tmp.sort(function(a, b){ return (a.time<b.time)?1:-1; });
			return tmp;
		}
	})
	.filter('buildsby',function(){
		return function (items, group) {
			var tmp = [];
			for(var i in items){
				var item = items[i];
					if (group)
					{
						if (item['group']==group) 
							tmp.push(item);
					}
					else
					{
						if ((item['group']!=2) && (item['group']!=3))
							tmp.push(item);
					}
			}
			//tmp.sort(function(a, b){ return (a.time<b.time)?1:-1; });
		    return tmp;
		}
	})
	.filter('reverse', function() {
	  return function(items) {
		  if (undefined==items) return undefined;
		return items.slice().reverse();
	  };
	})
	.filter('ts2human',function(){
		return function(ts, toTime){
			return ts2h(ts, toTime);
		}
	})
	.filter('count2human',function(){
		return function(ts){
			return count2h(ts);
		}
	});

// used in extended filters for finding list of values binded to column
function find(arr,name) {
  for(var i=0; i<arr.length; i++)
    if (arr[i].name==name) return arr[i].list;
};

function count2h(count, shownull)
{
	if (true!=shownull) shownull=false;
		
	if (0==count)
		if (shownull) return '0';
		else return '';
		
	if (count<1000) return count;
	return Math.ceil(count/1000)+'k';
}

function ts2h(ts, isTimer)
{
	if (undefined==ts) ts=0;
	
	if (ts<10) return 'меньше минуты';
	ts=Math.ceil(ts);
	
	if (0==ts) return '';
	
	if (ts>315360000) // 10 year
	{
		if (true==isTimer) return '';
		var d=new Date(ts*1000);
		var ret='';
		var h=d.getHours();
		var m=d.getMinutes();
		if (m<10) m='0'+m;
		ret=h+':'+m;
		return ret;
	}
	
	var d=0;
	var h=0;
	var m=0;
	var s=0;
	
	if (ts>=86400) { d=Math.floor(ts/86400); ts-=d*86400; }
	if (ts>=3600)  { h=Math.floor(ts/3600); ts-=h*3600; }
	if (ts>=60)    { m=Math.floor(ts/60); ts-=m*60; }
	s=Math.ceil(ts);
	
	var ret='';
	if (d) ret+=d+'д ';
	if (h) ret+=h+'ч ';
	if (!d) ret+=(m<10?('0'+m):m);
	if (d+h==0)
		ret+=":"+(s<10?'0'+s:s);
	else
		if (!d)
			ret+='м';
	
	return ret;
}