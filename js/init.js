'use strict';
app.controller('Login', LoginCtrl);
app.controller('Notice', NoticeCtrl);
app.controller('Game', GameCtrl);

//'greatkings.services','greatkings.filters','greatkings.directives','greatkings.httpProvider','LocalStorageModule']);
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/cities',			{controller: CityCtrl, templateUrl: 'city/index.html', reloadOnSearch: false})
        .when('/city/:id',			{controller: CityCtrl, templateUrl: 'city/view.html', reloadOnSearch: false})
        .when('/city/:id/res',		{controller: CityCtrl, templateUrl: 'city/res.html', reloadOnSearch: false})
        .when('/city/:cid/builds',	{controller: BuildCtrl, templateUrl: 'city/builds.html', reloadOnSearch: false})
        .when('/city/:cid/canbuild',{controller: CanBuildCtrl, templateUrl: 'city/canbuild.html', reloadOnSearch: false})
        .when('/build/:id',			{controller: BuildCtrl, templateUrl: 'build/view.html', reloadOnSearch: false})
        .when('/buildtype/:id',		{controller: CanBuildCtrl, templateUrl: 'build/buildtype.html', reloadOnSearch: false})
        .when('/heroes',			{controller: HeroCtrl, templateUrl: 'hero/index.html', reloadOnSearch: false})
        .when('/hero/:id',			{controller: HeroCtrl, templateUrl: 'hero/view.html', reloadOnSearch: false})
        .when('/armies',			{controller: ArmyCtrl, templateUrl: 'army/index.html', reloadOnSearch: false})
        .when('/army/:id',			{controller: ArmyCtrl, templateUrl: 'army/view.html', reloadOnSearch: false})
        .when('/army/:id/attack',	{controller: ArmyAttackCtrl, templateUrl: 'army/attack.html', reloadOnSearch: false})
        .when('/army/:id/reconnoitre',	{controller: ArmyAttackCtrl, templateUrl: 'army/attack.html', reloadOnSearch: false})
        .when('/war/:id',			{controller: WarCtrl, templateUrl: 'war/view.html', reloadOnSearch: false})
		.when('/mails',				{controller: MailCtrl, templateUrl: 'mail/index.html', reloadOnSearch: false})
        .when('/mail/:id',			{controller: MailCtrl, templateUrl: 'mail/dialog.html', reloadOnSearch: false})
        .when('/chat/rooms',		{controller: ChatCtrl, templateUrl: 'chat/index.html', reloadOnSearch: false})
		.when('/chat/all',			{controller: ChatCtrl, templateUrl: 'chat/all.html', reloadOnSearch: false})
		.when('/chat/:id',			{controller: ChatCtrl, templateUrl: 'chat/room.html', reloadOnSearch: false})
		.when('/map',				{controller: MapCtrl, templateUrl: 'map/index.html', reloadOnSearch: false})
		.when('/map/:pos',			{controller: MapCtrl, templateUrl: 'map/index.html', reloadOnSearch: false})
        .when('/unitconveer',		{controller: UnitconveerCtrl, templateUrl: 'unitconveer/index.html', reloadOnSearch: false})
		.when('/unitconveer/:id',	{controller: UnitconveerCtrl, templateUrl: 'unitconveer/index.html', reloadOnSearch: false})
		.when('/units',				{controller: UnitCtrl, templateUrl: 'unit/index.html', reloadOnSearch: false})
        .when('/unit/:id',			{controller: UnitCtrl, templateUrl: 'unit/view.html', reloadOnSearch: false})
		.when('/diplomat',			{controller: DiplomatCtrl, templateUrl: 'diplomat/index.html', reloadOnSearch: false})
		.when('/diplomat/clan',		{controller: DiplomatCtrl, templateUrl: 'diplomat/clan.html', reloadOnSearch: false})
		.when('/diplomat/gildia',	{controller: DiplomatCtrl, templateUrl: 'diplomat/gildia.html', reloadOnSearch: false})
		
		
        .when('/login',				{controller: LoginCtrl, templateUrl:'site/login.html', reloadOnSearch: false})
        .when('/logout',			{controller: LoginCtrl, templateUrl:'site/logout.html', reloadOnSearch: false})

        .otherwise({redirectTo: '/cities'});
}]);


/*
app.controller('StartCtrl',function($rootScope, $http){
	$rootScope.mails={};
});
app.run(function($rootScope, $http){
	$rootScope.checkLogin=function($rootScope, $location)
	{
		if (undefined==$rootScope.myuser.id)
			;//$location.path("/login");
	}
});
//app.run(function(GkData) {GkData.init()});
*/