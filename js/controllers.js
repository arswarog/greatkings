'use strict';

function LoginCtrl($scope, $routeParams, GkData) {
	$scope.remember=true;
	$scope.login='';
	$scope.password='';
	$scope.goLogin=function() {
		GkData.login($scope.login, $scope.password);
	};
	$scope.goLogout=function()
	{
		GkData.logout();
	};	
};

function NoticeCtrl($scope, $rootScope, $routeParams, $location, GkData) {
	$scope.goYes=function() {
		GkData.noticeYes();
	};
	$scope.goNo=function() {
		GkData.noticeNo();
	};
};

function GameCtrl($scope, $rootScope, $routeParams, GkData) {
	$scope.resetState=function() {
		GkData.resetState();
	};
};

function ChatCtrl($scope, $routeParams, GkData) 
{
	$scope.sending=false;
	$scope.roomid=0;
	var room=$routeParams.id;
	if (undefined==room) room=0;
	
	$scope.to=function(rid, uid, nick)
	{
		if (undefined==rid) return;
		$scope.roomid=rid;
		if (undefined!=uid)
		{
			$scope.touid=uid;
			$scope.tonick=nick;
		}
		else
		{
			$scope.touid=0;
			$scope.tonick="";
		}
	}
	$scope.chroom=function(id) { $scope.roomid=id; }
	$scope.send=function(to)
	{
		$scope.sending=true;
		if (0==to) to=$routeParams.id;
		//console.log('**chat send to '+to);
		GkData.query('chatroom', to, 'send', {text: $scope.text, to: $scope.touid}, function(data){
			//GkData.find('chatlist', room, function(list) { $scope.chatlist=list});
			//GkData.find('chat', room, function(list) { $scope.chatlogs=list});
			$scope.sending=false;
			$scope.text='';
			$scope.touid=0;
			$scope.tonick='';
		}, true);
	}
	$scope.options=function(room)
	{
		//alert(room);
	}
	
	GkData.update(function(){
		GkData.find('chatroom', room, function(list) { $scope.chatlist=list});
		if (room)
		{
			GkData.get('chatroom', room, 'full', function(data) { $scope.room=data});
			GkData.find('chat', {room: room}, function(list) { $scope.chatlogs=list});
		}
		else
			GkData.find('chat', {}, function(list) { $scope.chatlogs=list});
		$scope.loading=false;
	});
}

function BuildCtrl($scope, $routeParams, GkData) {
    $scope.group=2;
	$scope.setGroup=function(gr) { $scope.group=gr; }
	$scope.link=function(l)
	{
		$scope.pageloading=true;
		GkData.directQuery('build', $routeParams.id, 'page', {page: l}, function(data)
		{
			$scope.buildpage=data.page;
			$scope.pageloading=false;
		});
	};
	$scope.op=function(bid, act)
	{
		GkData.query('build', bid, act, {}, function(){
			GkData.get('build', bid, function(data){
				$scope.build=data;
			});
		}, true);
	};
	//GkData.update(function(){
		if ($routeParams.cid)
		{
			GkData.update(function(){
				var cityid=$routeParams.cid;
				GkData.get('city', cityid, 'full', function(data){
					$scope.city=data;
				});
				GkData.query('city', cityid, 'builds', {cityid: cityid}, function(){
					GkData.find('build', {cityid: cityid}, function(data){
						$scope.builds=data;
					});
				});
			});
		}
		if ($routeParams.id)
		{
			GkData.update(function(){
				GkData.get('build', $routeParams.id, 'full', function(data){
					$scope.build=data;
					GkData.get('city', data.cityid, 'full', function(city){
						$scope.city=city;
					});
				});
			});
			GkData.get('build', $routeParams.id, 'full', function(data){
				$scope.link('index');
			});
		}
	//});
}

function CanBuildCtrl($scope, $routeParams, GkData) {
    $scope.group=2;
	$scope.setGroup=function(gr) { $scope.group=gr; };
	if ($routeParams.cid)
	{
		var cityid=$routeParams.cid;
		GkData.get('city', cityid, 'canbuild', function(data){
			$scope.city=data;

		}, true);
	}
	
	$scope.building=function (type)
	{
		var cityid=$routeParams.cid;
		alert(type);
		GkData.query('city', cityid, 'build', {buildtype: type}, function(){
			
			/*GkData.find('build', {cityid: cityid}, function(data){
				$scope.builds=data;
			});*/
		});
	}
	
		/*if ($routeParams.id)
		{
			GkData.update(function(){
				GkData.get('build', $routeParams.id, 'full', function(data){
					$scope.build=data;
					GkData.get('city', data.cityid, 'full', function(city){
						$scope.city=city;
					});
				});
			});
			GkData.get('build', $routeParams.id, 'full', function(data){
				$scope.link('index');
			});
		}*/
	//});
}

function CityCtrl($scope, $rootScope, $routeParams, $location, GkData) {
	$scope.$watch('place', function(val) {
		//$location.path('/map/'+val);
	});
	$scope.place=undefined;
	GkData.update(function()
	{
		if ($routeParams.id)
		{
			GkData.get('city', $routeParams.id, 'full', function(data){
				$scope.city=data;
				GkData.get('army', $scope.city.armyid, 'full', function(data){
					$scope.cityarmy=data;
				})
			});
		}
		else
			GkData.find('city', {uid: 'myid'}, function(data) {
				$scope.cities=data;
			});
	});
}

function UnitCtrl($scope, $rootScope, $routeParams, GkData) {
	if ($routeParams.id)
	{
		GkData.get('unit', $routeParams.id, 'full', function(data){
			$scope.unit=data;
		});
	}
	else
		GkData.query('unit', 0, 'all', {}, function(){
			GkData.find('unit', {race: 'myrace'}, function(data) {
				$scope.units=data;
			});
		});
}

function UnitconveerCtrl($scope, $routeParams, $location, GkData) {
	function setcid(cid)
	{
		$scope.cityid=cid;
		if (undefined==$scope.cityid) $scope.cityid=$routeParams.id;
		if (undefined==$scope.cityid) $scope.cityid=0;
		GkData.find('city', {uid: 'myid'}, function(data){
			$scope.citylist=data;
		});
		if ($scope.cityid)
		{
			GkData.get('city', $scope.cityid, 'full', function(data){
				$scope.city=data;
			});
			GkData.update(function(){
				GkData.find('unitconveer', {cityid: $scope.cityid}, function(data){
					$scope.unitconveers=data;
				});
			});
			GkData.query('unit', $scope.cityid, 'city', {}, function(){
				GkData.find('unit', {race: 'myrace'}, function(data) {
					$scope.units=data;
				});
			}, true);
		}
		else
		{
			GkData.update(function(){
				GkData.find('unitconveer', {}, function(data){
					$scope.unitconveers=data;
				});
			});
			GkData.query('unit', $scope.cityid, 'city', {}, function(){
				GkData.find('unit', {race: 'myrace'}, function(data) {
					$scope.units=data;
				});
			});
		}
	};
	$scope.newconveer=function(uid, count)
	{
		GkData.query('unitconveer', $scope.cityid, 'create', {unit: uid, count: count}, function(){
		},true);
	}
	setcid($routeParams.id);
	GkData.update(setcid);
}

function HeroCtrl($scope, $routeParams, GkData) {
	if ($routeParams.id)
        GkData.get('hero', $routeParams.id, 'full', function(data){
			$scope.hero=data;
		});
	else
		GkData.find('hero', {uid: 'myid'}, function(data) {
			$scope.heroes=data;
		});
}

function ArmyCtrl($scope, $routeParams, GkData) {
	if ($routeParams.id)
		GkData.update(function(){
			GkData.get('army', $routeParams.id, 'full', function(data){
				$scope.army=data;
			}, true);
		});
	else
	{
		GkData.update(function(){
			GkData.find('army', {uid: 'myid', type: 'army'}, function(data) {
				$scope.armies=data;
			})
			GkData.find('general', {uid: 'myid'}, function(data) {
				$scope.generals=data;
			})
			GkData.find('war', {}, function(data) {
				$scope.wars=data;
			})
		})
	}
		
}

function ClanCtrl($scope, $routeParams, GkData)
{	
	GkData.update(function(){
		GkData.get('clan', $routeParams.id, 'full', function(data){
			$scope.clan=data;
		});
	});
}

function DiplomatCtrl($scope, $routeParams, GkData)
{	
	GkData.update(function(){
		GkData.get('my', 'clan', 'full', function(data){
			$scope.myclan=data;
		});
		GkData.get('my', 'gildia', 'full', function(data){
			$scope.mygildia=data;
		});
	});
}

function WarCtrl($scope, $routeParams, GkData)
{	
	GkData.update(function(){
		GkData.get('war', $routeParams.id, 'full', function(data){
			$scope.war=data;
		});
	});
	$scope.items=[];
	$scope.items[101]=
		{
			img: 'explosion2',
			w: 64, h: 64, 
			cadr: 0, cadrs: 0,
			anim: 10, anims: 15, ad:0, d:1,
			x: 60, y: 20
			
		};
	$scope.items[102]=
		{
			img: 'explosion2',
			w: 64, h: 64, 
			cadr: 0, cadrs: 0,
			anim: 0, anims: 15, ad:0, d:2,
			x: 20, y: 120
			
		};
	$scope.items[103]=
		{
			img: 'explosion2',
			w: 64, h: 64, 
			cadr: 0, cadrs: 0,
			anim: 3, anims: 15, ad:0, d:3,
			x: 120, y: 50
			
		};
	$scope.items[100]=
		{	
			img: 'explosion4',
			w: 142, h: 142, 
			cadr: 0, cadrs: 0,
			anim: 0, anims: 4, ad:0, d: 5,
			x: 80, y: 50
			
		};
	/*
	for (var i=0; i<10; i++)
	for (var j=0; j<10; j++)
{
		$scope.items[j*10+i]={	img: 'explosion3',
			w: 35, h: 35, 
			cadr: 0, cadrs: 0,
			anim: ((i*10+j)*7)%30, anims: 30, ad:0, d:1,
			x: i*25, y: j*25
			
		};
}	*/	
	$scope.st=0;
	GkData.timer(function(){
		
		for(var i in $scope.items)
		{
			var item=$scope.items[i];
			item.ad++;
			if (item.ad>=item.d) { item.ad=0; item.anim++; }
			if (item.anim>=item.anims) item.anim=0;
			$scope.items[i]=item;
		}
		
		/*$scope.st++;
		if ($scope.st>40) $scope.st=0;
		var x=$scope.st*35;
		$scope.anime="url(/img/proba/explosion3.png) -"+x+"px 0 no-repeat"

		var y=Math.floor($scope.st/5)
		var x=$scope.st-y*5;
		$scope.anime1="url(/img/proba/explosion2.png) -"+x*64+"px -"+y*64+"px no-repeat"

		var y=Math.floor($scope.st/2/5)
		var x=Math.floor($scope.st/2)-y*5;
		$scope.anime2="url(/img/proba/explosion4.png) -"+x*142+"px -"+y*142+"px no-repeat"

		var y=Math.floor($scope.st/2/5)
		var x=Math.floor($scope.st/2)-y*5;
		$scope.anime3="url(/img/proba/explosion5.png) -"+x*256+"px -"+y*256+"px no-repeat"

		var y=Math.floor($scope.st/6)
		var x=Math.floor($scope.st)-y*6;
		$scope.anime4="url(/img/proba/b.png) -"+x*67+"px -"+y*66+"px no-repeat"
		$scope.anime5="url(/img/proba/c.png) -"+x*67+"px -"+y*66+"px no-repeat"*/

	});
	$scope.action=function(name)
	{
		GkData.query('war', $scope.war.id, 'action', {action: name}, function(){ 
			GkData.get('war', $routeParams.id, 'full', function(data){
				$scope.war=data;
			});
		}, true);
	}
}

function ArmyAttackCtrl($scope, $routeParams, $location, GkData) {
	$scope.mode=$location.path().substr($location.path().lastIndexOf('/')+1);
	$scope.army={
		id: $routeParams.id
	};
	GkData.update(function(){
		GkData.get('army', $routeParams.id, 'full', function(data){
			$scope.army=data;
		}, true);
		GkData.find('army', {uid: 'myid'}, function(data){
			$scope.armies=data;
		});
	});
	$scope.goAttack=function(id){
		GkData.query('army', $scope.army.id, 'attack', {from: id}, function(data){
			if ("false"!=data.return)
				$location.path('/war/'+data.return);
		}, true);
	};
}


/// FIXME сохранять последнюю позицию карты в куки
function MapCtrl($scope, $rootScope, $routeParams, GkData) {
	var pos=$routeParams.pos;
	$scope.route=pos;
	$scope.view='';
	GkData.update(function(){
		GkData.find('army', {uid: 'myid'}, function(data){
			$scope.myarmies=data;
		});
		GkData.find('army', {pos: $scope.place}, function(data){
			$scope.armies=data;
		});
	});
	$scope.$watch('place', function(val){$scope.review(val)});
	$scope.review=function(val)
	{
		$scope.armieshide=false;
		$scope.p=$scope.p;
		GkData.find('city', {pos: $scope.place}, function(data){
			$scope.city=data[0];
		});
		GkData.find('army', {pos: val}, function(data){
			$scope.armies=data;
			$scope.view='';
		});
	};
	$scope.$watch('view', function(val){
		if ('army'==val)
		{
			$scope.waytoloading=true;
			GkData.query('army', 0, 'wayto', {target: $scope.place}, function(data){
				GkData.find('army', {uid: 'myid'}, function(data){
					$scope.myarmies=data;
				});
				$scope.waytoloading=0;
			}, true);
		}
	});
	if (undefined==$routeParams.pos)
        pos='40:14:0';
	$scope.p=pos;
	
	$scope.armygo=function(aid)
	{
		for(var i in $scope.myarmies)
		{
			if ($scope.myarmies[i]['id']==aid)
				$scope.myarmies[i]['upd']=1;
		}
		
		GkData.query('army', aid, 'go', {target: $scope.place}, function(){
			GkData.find('army', {uid: 'myid'}, function(data){
				$scope.myarmies=data;
				$scope.review($scope.place);
			});
		}, true);
		return false;
	}
}

function MailCtrl($scope, $rootScope, $routeParams, $http, $location) {
	$rootScope.checkLogin($rootScope, $location);
    if ($routeParams.id)
	{
		var uid=$routeParams.id;
		
		if (undefined==$rootScope.dialogs[uid] || undefined==$rootScope.dialogs[uid].mails)
		{
			$scope.loading=true;
			$http.get('/mail/'+uid).then(function(r) {
				apply(r.data, $rootScope);
				$scope.dialog=$rootScope.dialogs[uid];
				$scope.loading=false;
			});
		} 
		else
		{
			$scope.dialog=$rootScope.dialogs[uid];
			$scope.loading=false;
		}
	}
}

/*
function MenuCtrl($rootScope, $http){
    
    $rootScope.$on("init", function(ev, $http){
      console.log("loading City data");
      $rootScope.update('update', $http);
    });
      //$rootScope.foobar = data + " " + $scope.foobar;
   // });
     
}*/

/*
//<![CDATA[
angular.module('demoModule',['LocalStorageModule'])
        .config(['localStorageServiceProvider',
    function(localStorageServiceProvider)
    {localStorageServiceProvider.setPrefix('demoPrefix');}])
        .controller('DemoCtrl',['$scope','localStorageService',
    function($scope,localStorageService)
    {$scope.$watch('localStorageDemo',
        function(value){localStorageService.set('localStorageDemo',value);$scope.localStorageDemoValue=localStorageService.get('localStorageDemo');});$scope.storageType='Local storage';if(localStorageService.getStorageType().indexOf('session')>=0){$scope.storageType='Session storage';}
if(!localStorageService.isSupported){$scope.storageType='Cookie';}}]);
//]]>
*/

/*
function CityCtrl($scope, $routeParams, $location, Cities) {
	$scope.item = Cities.get({id:$routeParams.id});
	//$scope.item = {id:0,title:'example'};//Cities.get({id:$routeParams.id});
	$scope.save = function() {
		$scope.item.$save({id:$scope.item.id}, function(){ $location.path('/cities'); });
	};
	// TODO wysiwyg http://deansofer.com/posts/view/14/AngularJs-Tips-and-Tricks#wysiwyg
}*/

function ListCtrl($scope, Items, Data) {
	$scope.Math = Math;
	$scope.items = Items.query(function(data){
		$scope.paginator.setPages($scope.items.length);
		var i = 0;
		angular.forEach(data, function(v,k) { data[k]._id = i++; });
	});
	$scope.categories = Data('categories');
	$scope.answerers  = Data('answerers');

	$scope.selected  = [];
	$scope.paginator = {
		count: 3,
		page:  1,
		pages: 1,
		setPages: function(itemsCount){ this.pages = Math.ceil(itemsCount/this.count); }
	};

	$scope.tablehead = [
		{name:'title',    title:"Заголовок",  sort:-2},
		{name:'category', title:"Категория",  sort:1, list:$scope.categories},
		{name:'answerer', title:"Кому задан", list:$scope.answerers},
		{name:'author',   title:"Автор"},
		{name:'created',  title:"Задан"},
		{name:'answered', title:"Отвечен"},
		{name:'shown',    title:"Опубликован"}
	];

	$scope.sortBy = function() {
		var order = [];
		angular.forEach($scope.tablehead, function(h){
			if (h.sort>0) order[h.sort-1] = h.name;
			if (h.sort<0) order[Math.abs(h.sort)-1] = '-'+h.name;
		});
		return order;
	};
	$scope.sortReorder = function(col,e) {
		if (e.shiftKey) {
			var sortIndex = 0;
			angular.forEach($scope.tablehead, function(el) {
				if (Math.abs(el.sort)>sortIndex) sortIndex = Math.abs(el.sort);
			});
			angular.forEach($scope.tablehead, function(el) {
				if (el.name==col) el.sort = el.sort?-el.sort:sortIndex+1;
			});
		} else {
			angular.forEach($scope.tablehead, function(el) {
				if (el.name==col) el.sort = el.sort>0?-1:1; else el.sort = null;
			});
		}
	};

	$scope.disableItem = function() {
		var item = this.item;
		Items.toggle({id:item.id}, function(data) { if (data.ok) item.shown = item.shown>0?0:1; });
	};
	$scope.deleteItem = function(one) {
		if (one) {
			var _id = $scope.selected[0];
			Items['delete']({id:$scope.items[_id].id}, function() {
				$scope.items.splice(_id,1);
				$scope.selected = [];
			});
		} else {
				var ids = [];
				angular.forEach($scope.selected, function(_id) { ids.push($scope.items[_id].id); });
				Items['delete']({ids:ids}, function(){
					angular.forEach($scope.selected, function(_id) { $scope.items.splice(_id,1); });
					$scope.selected = [];
				});
			}
	};
	$scope.selectItem = function(e) {
		if ((e.target||e.srcElement).tagName!='TD') return;
		var state = this.item.selected = !this.item.selected, _id = this.item._id;
		if (state) $scope.selected.push(_id);
			else angular.forEach($scope.selected, function(v,k) {
			       if (v==_id) { $scope.selected.splice(k,1); return false; }
			     });
	};
	$scope.$watch('items',function(newValue, oldValue) {
		if (newValue!==oldValue) $scope.paginator.setPages($scope.items.length);
	});
	$scope.$watch('paginator.page',function(page,old) {
		var newPage = page;
		if (page<1) newPage = 1;
		if (page>$scope.paginator.pages) newPage = old;
		if (typeof(newPage)=='string') newPage = +newPage.replace(/[^0-9]/,'');
		if (page!==newPage) $scope.paginator.page = newPage;
		angular.forEach($scope.items, function(v,k) { $scope.items[k].selected = false; });
	});
}

function EditCtrl($scope, $routeParams, $location, Items, Data) {
	$scope.item = Items.get({id:$routeParams.id});
	$scope.categories = Data('categories');
	$scope.answerers  = Data('answerers');
	$scope.save = function() {
		$scope.item.$save({id:$scope.item.id}, function(){ $location.path('/list'); });
	};
	// TODO wysiwyg http://deansofer.com/posts/view/14/AngularJs-Tips-and-Tricks#wysiwyg
}

function NewCtrl($scope, $location, Items, Data) {
	$scope.item = {id:0,category:'',answerer:'',title:'',text:'',answer:'',author:''};
	$scope.categories = Data('categories');
	$scope.answerers  = Data('answerers');
	$scope.save = function() {
		Items.create($scope.item, function(){ $location.path('/list'); });
	};
}
